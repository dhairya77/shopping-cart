package sheridan;
import java.util.ArrayList;
import java.util.List;
public class Cart {
    private List <Product> products = new ArrayList<>( );
    private PaymentService service;
    public void setPaymentService( PaymentService service ) {
        this.service  = service;
    }
    public void addProduct( Product product ) {
        products.add( product );
    }  
    public void payCart( ) {
        double total = 0;
        double discount = 20;
       for ( Product product : products ) {
           total = product.getPrice() - discount;
       }
        service.processPayment( total );
    }
}
