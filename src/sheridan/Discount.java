/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package sheridan;

/**
 *
 * @author DHAIRYA
 */
public class Discount {
    public double percentage;
    public double amount;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Discount(double percentage, double amount) {
        this.percentage = percentage;
        this.amount = amount;
    }

    public Discount(double percentage) {
        this.percentage = percentage;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }
    
    public void calculateDiscount(){
    }
    
    
}
