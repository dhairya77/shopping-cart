package sheridan;
public abstract class PaymentService {   
    public abstract void processPayment( double amount );
}
