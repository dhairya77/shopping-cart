package sheridan;
public class Factory {
    private double discount;

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public Factory(double discount) {
        this.discount = discount;
    }
    
}
